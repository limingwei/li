目录结构：
* 框架源码 -> framework
* 扩展工具 -> more
* 尚未完成 -> dev
* 单元测试 -> test
* 示例项目 -> demo
* WebRoot -> WebContent

by li limingwei@mail.com
* QQ: 416133823
* Q群: 254860190

javadoc
* li-framework http://limingwei.github.com/liz 
* li-more http://limingwei.github.com/liz/more 

下载
* li-framework.jar http://limingwei.github.com/liz/downloads/li-framework.jar
* li-more.jar http://limingwei.github.com/liz/downloads/li-more.jar
* cglib-nodep.jar http://limingwei.github.com/liz/downloads/cglib-nodep-2.2.3.jar
* 文档 http://limingwei.github.com/liz/downloads/li.pdf

~!@#$%^&*()_+{}|:"<>?`-=[]\;',.//*-+.
* 介绍 http://limingwei.github.com/li
* 提问&讨论   https://github.com/limingwei/li/issues
* 更新记录   https://github.com/limingwei/li/wiki/changelog